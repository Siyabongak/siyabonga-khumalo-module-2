void main() {
  // List of winning apps
  var winningApps = [
    "Ambani",
    "EasyEquities",
    "Naked Insurance",
    "Khula",
    "Standard Bank Shyft",
    "Domestly",
    "WumDrop",
    "Live Inspect",
    "SnapScan",
    "FNB Banking App"
  ];

  // For loop to print the winning apps
  for (var app in winningApps) {
    print("The winning app is $app");
  }

  //Winning app of 2017 and 2018
  print("The winning app for 2017 is ${winningApps[4]}");
  print("The winning app for 2018 is ${winningApps[3]}");
  //The total number of apps
  print("The total number of apps is ${winningApps.length}");
}
