void main() {
  //Our first object instance
  var ambani = App();
  ambani.name = "Ambani Africa";
  ambani.sector = "Education";
  ambani.developer = "Mukundi Lambani";
  ambani.winningYear = 2021;
  ambani.printStatements();
}

class App {
  String? name;
  String? sector;
  String? developer;
  int? winningYear;

  //This method prints the app data and capitalises the app name
  void printStatements() {
    name = name?.toUpperCase();

    print("The app name is $name ");
    print("$name is in the $sector sector.");
    print("$name was developed by $developer ");
    print("$name was the winning app for $winningYear");
  }
}
